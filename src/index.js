import React from 'react'
import ReactDOM from 'react-dom'
import reportWebVitals from 'reportWebVitals'

import routes from "routes"
import history from 'utils/history'
import { renderRoutes } from 'utils/router'
import { Router } from "react-router-dom"

import 'assets/styles/boostrap1.min.css'
import 'assets/styles/boostrap2.min.css'
import 'assets/styles/custom.scss'

ReactDOM.render(
  <React.StrictMode>
    <Router history={history}>
      {renderRoutes(routes)}
    </Router>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
