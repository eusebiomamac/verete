import { useState, useCallback } from 'react'
import debounce from 'lodash/debounce'

function useSearch(initialSearch = '', delay = 1000) {
  const [search, setSearch] = useState(initialSearch)

  const debounceSearch = useCallback(debounce(onSearch, delay), [])
  return [search, debounceSearch]

  function onSearch(value) {
    setSearch(value)
  }
}

export default useSearch
