import { useState, ReactNode } from 'react'
import axios from 'utils/axios'

export default function useMutation(params, initialData = []) {
  const [loading, setLoading] = useState(false)
  const [data, setData] = useState(initialData)
  const [error, setError] = useState()

  const state = {
    loading,
    data,
    error,
  }

  return [state, mutate]
  async function mutate(params2 = {}) {
    setLoading(true)
    setData(null)

    const body = {}
    const allParams = { ...params, ...params2 }
    const {
      data,
      method = 'POST',
      onSuccess = () => {},
      onError = () => {},
    } = allParams

    let { url } = allParams
    url = [url, ['delete', 'put'].includes(method.toLowerCase()) && data.id].filter(Boolean).join('/')

    if (method !== 'GET')
      body.data = data

    const response = await axios({
      ...body,
      method,
      url,
    }).catch((err) => {
      setData(null)
      onError(err.response?.data)
      setError(err.response?.data)
      return { error: err.response?.data }
    })

    setLoading(false)

    if (response && !response.error) {
      setError(null)
      setData(response)
      onSuccess(response)
    }

    return response
  }
}
