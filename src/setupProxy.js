const { createProxyMiddleware } = require('http-proxy-middleware')

module.exports = function(app) {
  app.use(
    '/api/image',
    createProxyMiddleware({
      target: process.env.REACT_APP_ASSET_URL,
      pathRewrite: { '^/api/image': '' },
      changeOrigin: true,
    }),
  )
  app.use(
    '/api',
    createProxyMiddleware({
      target: process.env.REACT_APP_API_URL,
      pathRewrite: { '^/api': '' },
      changeOrigin: true,
    }),
  )
}
