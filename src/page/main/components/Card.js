import React, { useMemo } from 'react'
import dayjs from 'dayjs'
import placeholder from 'assets/image/placeholder.jpg'

function Card(props) {
  const { data, href, links } = props

  const thumbnail = useMemo(() => {
    return links?.find(({ render }) => render === 'image')
  }, [links])

  const card = useMemo(() => {
    const [info] = data
    return info
  }, [data])

  return (
    <div className="col-md-3">
      <div className="card">
        <img className="card-img-top" src={(thumbnail?.href || placeholder)} alt="Image placeholder" />
        {/*<div className="card-body">
            <h5 className="h2 card-title mb-0">{card?.title}</h5>
            <small className="text-muted">by {card?.secondary_creator} on {dayjs(card?.date_created).format('MMM D, YYYY h:mm A')}</small>
            <p className="card-text mt-4">{card?.description_508}</p>
        </div>*/}
      </div>
    </div>
  )
}

export default Card
