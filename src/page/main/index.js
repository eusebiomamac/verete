import React, { useMemo, Fragment, useState, useEffect } from 'react'
import useSearch from 'hooks/useSearch'
import useMutation from 'hooks/useMutation'
import trim from 'lodash/trim'
import delay from 'lodash/delay'
import queryString from 'qs'
import history from 'utils/history'

import Card from 'page/main/components/Card'

export default function MainPage() {
  const [keyword, onSearch] = useSearch()
  const [updateId, setForceUpdate] = useState()

  const params = useMemo(() => queryString.stringify({
    q: keyword,
    media_type: 'image, video',
    year_start: 1920,
    year_end: 2021,
  }), [keyword])

  const [queryState, onMutate] = useMutation({
    url: `/search?${params}`,
    method: 'GET'
  })

  const { data, loading } = queryState

  useEffect(() => {
    onMutate({
      url: `/image/popular.json`,
      method: 'GET'
    })
  }, [])
  return (
    <Fragment>
      <div className="container-fluid mt-6">
        <div className="row">
          <div className="col-md-5 search">
            <div className="form-group">
              <div className="input-group input-group-merge">
                <div className="input-group-prepend">
                  <span className="input-group-text">
                    <i className="fa fa-search"></i>
                  </span>
                </div>
                <input className="form-control" placeholder="search..." type="text" onKeyUp={handleKeyDown} />
              </div>
            </div>
            <button type="button" className="btn btn-primary" onClick={onMutate}>Find</button>
          </div>
        </div>
      </div>

      <div className="container-fluid mt-6">
        <div className="row">
          {loading && (
            <div>Loading, please wait...</div>
          )}
          {!loading && Boolean(!data?.collection?.items.length) && (
            <div>No results found</div>
          )}
          {!loading && data?.collection?.items?.map((item, index) => (
            <Card
              key={index}
              data={item.data}
              href={item.href}
              links={item.links}
            />
          ))}
        </div>
      </div>
    </Fragment>
  )

  function handleKeyDown(event) {
    const value = event?.target?.value

    if (value)
      onSearch(value)

    if (value && event.keyCode === 13)
      handleEnter(value)
  }

  function handleEnter(keyword) {
    const params = queryString.stringify({
      q: keyword,
      media_type: 'image, video',
      year_start: 1920,
      year_end: 2021,
    })

    onMutate({ url: `/search?${params}` })
  }
}


