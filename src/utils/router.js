import React, { Suspense, Fragment } from 'react'
import { Route, Switch, Redirect  } from "react-router-dom"

export function renderRoutes(routes) {
  if (!routes)
    return null

  return (
    <Fragment>
        <Suspense fallback={<div>Loading, please wait...</div>}>
          <Switch>
            {routes.map(({ Component, ...route }, index) => (
              <Route { ...route } render={props => (
                  <Component route={route} { ...props }/>
                )}
              />
            ))}
            <Redirect to="/notfound" />
          </Switch>
        </Suspense>
    </Fragment>
  )
}
