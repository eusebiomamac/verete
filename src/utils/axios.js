import axios from 'axios'

const baseURL = '/api'
const axiosInstance = axios.create({
  baseURL,
})

axiosInstance.interceptors.request.use(
  function(config) {

    return config
  }, err => Promise.reject(err))

axiosInstance.interceptors.response.use(
  function(response) {
    return response.data
  }, err => Promise.reject(err),
)

export default axiosInstance
