import React, { lazy } from 'react'

const MainPage = lazy(() => import('page/main'))

const NotFound = () => (
  <div className="not-found">
    <p className="title"><b>404: </b>Page not Found</p>
    <p className="desc">The page you are looking for cannot be found</p>
  </div>
)

const routes = [
  {
    path: "/",
    name: "Main page",
    key: "main-page",
    Component: MainPage,
    exact: true
  },
  {
    path: "/notfound",
    name: "Not Found",
    key: "route-notfound",
    Component: NotFound,
    exact: true
  }
]

export default routes
